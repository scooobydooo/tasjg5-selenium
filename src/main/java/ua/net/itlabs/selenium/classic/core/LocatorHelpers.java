package ua.net.itlabs.selenium.classic.core;

import org.openqa.selenium.By;

public class LocatorHelpers {
    public static By by(String cssSelector) {
        return By.cssSelector(cssSelector);
    }
}
