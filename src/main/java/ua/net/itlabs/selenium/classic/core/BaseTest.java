package ua.net.itlabs.selenium.classic.core;

import org.junit.After;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BaseTest extends ConciseAPI {
    protected WebDriver driver = new FirefoxDriver();

    public WebDriver getWebDriver() {
        return driver;
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
