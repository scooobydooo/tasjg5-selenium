package ua.net.itlabs.selenium.classic.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ua.net.itlabs.selenium.classic.core.BasePage;

public class NewMailWidget extends BasePage {

    @FindBy(name = "to")
    WebElement to;
    @FindBy(name = "subjectbox")
    WebElement subject;
    @FindBy(css = "[role='textbox']")
    WebElement body;
    @FindBy(xpath = "//div[contains(text(), 'Send')]")
    WebElement sendButton;

    public NewMailWidget(WebDriver driver) {
        super(driver);
    }

    public void sendEmail(String to, String subject) {

        find(this.to).sendKeys(to);
        this.subject.sendKeys(subject);
        sendButton.click();
    }

    public void sendEmail(String to, String subject, String body) {
        find(this.to).sendKeys(to);
        this.subject.sendKeys(subject);
        this.body.sendKeys(body);
        sendButton.click();
    }
}
