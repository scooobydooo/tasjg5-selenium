package ua.net.itlabs.selenium.classic.core;

import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static ua.net.itlabs.selenium.classic.core.Configuration.timeout;
import static ua.net.itlabs.selenium.classic.core.CustomConditions.minimalSizeOf;
import static ua.net.itlabs.selenium.classic.core.CustomConditions.visible;

abstract public class ConciseAPI {

    abstract public WebDriver getWebDriver();

    public WebDriverWait hold() {
        return new WebDriverWait(getWebDriver(), timeout);
    }

    public void open(String url) {
        getWebDriver().get(url);
    }

    public WebElement get(List<WebElement> elements, int index) {
        hold().until(minimalSizeOf(elements, index + 1));
        return elements.get(index);
    }

    public <V> V waitUntil(Function<? super WebDriver, V> condition) {
        return hold().until(condition);
    }

    public <V> V assertThat(Function<? super WebDriver, V> condition) {
        return waitUntil(condition);
    }

    public WebElement find(By elementLocator) {
        return waitUntil(visible(elementLocator));
    }

    public WebElement find(WebElement webElement) {
        return waitUntil(visible(webElement));
    }

    public List<WebElement> find(List<WebElement> elements) {
        return waitUntil(visible(elements));
    }

}
