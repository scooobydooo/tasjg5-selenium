package ua.net.itlabs.selenium.classic.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class CustomConditions {
    public static ExpectedCondition<Boolean> textToBePresentInNthElement(
            final List<WebElement> elementsList, final int nthIndex, final String expectedText) {
        return new ExpectedCondition<Boolean>() {
            String actualText;

            public Boolean apply(WebDriver webDriver) {
                try {
                    actualText = elementsList.get(nthIndex).getText();
                    return actualText.contains(expectedText);
                } catch (IndexOutOfBoundsException e) {
                    return false;
                }
            }

            public String toString() {
                return String.format("Expected text: '%s'" +
                        "\nto be present in %s element" +
                        "\nof list: %s" +
                        "\nwhile actual text: '%s'", expectedText, nthIndex, elementsList, actualText);
            }
        };
    }

    public static ExpectedCondition<Boolean> minimalSizeOf(final List<WebElement> elements, final int expectedSize) {
        return new ExpectedCondition<Boolean>() {
            int actualSize;

            public Boolean apply(WebDriver webDriver) {
                try {
                    actualSize = elements.size();
                    return actualSize >= expectedSize;
                } catch (IndexOutOfBoundsException e) {
                    return false;
                }
            }

            public String toString() {
                return String.format("Expected minimal size of list '%s'" +
                        "\nto be: '%s'" +
                        "\nwhile actual is: '%s", elements, expectedSize, actualSize);
            }
        };
    }

    public static ExpectedCondition<Boolean> sizeOf(final List<WebElement> elements, final int expectedSize) {
        return new ExpectedCondition<Boolean>() {
            int actualSize;

            public Boolean apply(WebDriver webDriver) {
                try {
                    actualSize = elements.size();
                    return actualSize == expectedSize;
                } catch (IndexOutOfBoundsException e) {
                    return false;
                }
            }

            public String toString() {
                return String.format("Expected size of list '%s'" +
                        "\nto be: '%s'" +
                        "\nwhile actual is: '%s", elements, expectedSize, actualSize);
            }
        };
    }

    public static ExpectedCondition<WebElement> visible(final WebElement webElement) {
        return visibilityOf(webElement);
    }

    public static ExpectedCondition<WebElement> visible(final By elementLocator) {
        return visibilityOfElementLocated(elementLocator);
    }

    public static ExpectedCondition<List<WebElement>> visible(final List<WebElement> elements) {
        return visibilityOfAllElements(elements);
    }
}
