package ua.net.itlabs.selenium.classic.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ua.net.itlabs.selenium.classic.core.BasePage;

import java.util.List;

import static ua.net.itlabs.selenium.classic.core.LocatorHelpers.by;

public class GooglePage extends BasePage {

    @FindBy(name = "q")
    public WebElement searchField;

    @FindBy(css = ".srg .g")
    public List<WebElement> results;

    public GooglePage(WebDriver driver) {
        super(driver);
    }

    public void search(String text) {
        searchField.sendKeys(text, Keys.ENTER);
    }

    public void visit() {
        open("http://google.com/ncr");
    }

    public void followLink(int i) {
        get(results, i).findElement(by(".r a")).click();
    }
}
