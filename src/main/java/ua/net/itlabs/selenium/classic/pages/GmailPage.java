package ua.net.itlabs.selenium.classic.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ua.net.itlabs.selenium.classic.core.BasePage;

import java.util.List;

import static ua.net.itlabs.selenium.classic.core.CustomConditions.sizeOf;
import static ua.net.itlabs.selenium.classic.core.LocatorHelpers.by;

public class GmailPage extends BasePage {
    @FindBy(css = "[role='search'] input")
    public WebElement searchField;
    @FindBy(css = "[role='main'] tbody>tr")
    public List<WebElement> emails;
    @FindBy(css = "[role='main'] tbody>tr div>span>b")
    public List<WebElement> unreadEmailsSubjects;
    @FindBy(xpath = "//div[contains(text(), 'COMPOSE')]")
    public WebElement composeButton;


    public NewMailWidget mailWidget;

    public GmailPage(WebDriver driver) {
        super(driver);
        mailWidget = new NewMailWidget(driver);
    }

    public void visit() {
        open("https://mail.google.com");
    }

    public void signIn() {
        find(by("#Email")).sendKeys("itlabs.tasjg5@gmail.com" + Keys.ENTER);
        find(by("#Passwd")).sendKeys("itlabsguest" + Keys.ENTER);
    }

    public void refreshEmailList() {
        find(by(".asf.T-I-J3.J-J5-Ji")).click();
    }

    public void search(String expectedSubject) {

        find(searchField).sendKeys(expectedSubject + Keys.ENTER);
        find(unreadEmailsSubjects);
        assertThat(sizeOf(unreadEmailsSubjects, 1));
    }

}
