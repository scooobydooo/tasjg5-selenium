package ua.net.itlabs.selenium.simpler.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class CustomConditions {

    public static ExpectedCondition<Boolean> sizeOf(final By elementsLocator, final int expectedSize) {
        return new ExpectedCondition<Boolean>() {
            int actualSize;

            public Boolean apply(WebDriver webDriver) {
                try {
                    actualSize = webDriver.findElements(elementsLocator).size();
                    return actualSize == expectedSize;
                } catch (IndexOutOfBoundsException e) {
                    return false;
                }
            }

            public String toString() {
                return String.format("Expected size of list '%s'" +
                        "\nto be: '%s'" +
                        "\nwhile actual is: '%s", elementsLocator, expectedSize, actualSize);
            }
        };
    }

    public static ExpectedCondition<WebElement> visible(final By elementLocator) {
        return visibilityOfElementLocated(elementLocator);
    }

}
