package ua.net.itlabs.selenium.simpler.core;

import org.openqa.selenium.By;

public class LocatorHelpers {

    public static By by(String cssSelector) {
        return By.cssSelector(cssSelector);
    }
}
