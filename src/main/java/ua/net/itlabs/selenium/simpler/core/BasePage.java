package ua.net.itlabs.selenium.simpler.core;

import org.openqa.selenium.WebDriver;

public class BasePage extends ConciseAPI {

    private WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public WebDriver getWebDriver() {
        return driver;
    }


}
