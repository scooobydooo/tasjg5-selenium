package ua.net.itlabs.selenium.simpler.core;

import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import static ua.net.itlabs.selenium.simpler.core.CustomConditions.visible;

abstract public class ConciseAPI {

    abstract public WebDriver getWebDriver();

    public WebDriverWait hold() {
        return new WebDriverWait(getWebDriver(), Configuration.timeout);
    }

    public void open(String url) {
        getWebDriver().get(url);
    }

    public <V> V waitUntil(Function<? super WebDriver, V> condition) {
        return hold().until(condition);
    }

    public <V> V assertThat(Function<? super WebDriver, V> condition) {
        return waitUntil(condition);
    }

    public WebElement find(By elementLocator) {
        return waitUntil(visible(elementLocator));
    }

}
