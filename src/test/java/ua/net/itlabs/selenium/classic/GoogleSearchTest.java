package ua.net.itlabs.selenium.classic;

import org.junit.Test;
import ua.net.itlabs.selenium.classic.core.BaseTest;
import ua.net.itlabs.selenium.classic.core.CustomConditions;
import ua.net.itlabs.selenium.classic.pages.GooglePage;

import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

public class GoogleSearchTest extends BaseTest {
    GooglePage googlePage = new GooglePage(driver);

    @Test
    public void searchText() {
        googlePage.visit();
        googlePage.search("selenium");

        waitUntil(CustomConditions.textToBePresentInNthElement(googlePage.results, 0, "Selenium is a suite of tools"));
    }

    @Test
    public void testFollowLinks() {
        googlePage.visit();
        googlePage.search("selenium");
        googlePage.followLink(0);

        assertThat(titleIs("Selenium - Web Browser Automation"));
    }


}
