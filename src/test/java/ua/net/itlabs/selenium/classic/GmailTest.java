package ua.net.itlabs.selenium.classic;

import org.junit.Test;
import ua.net.itlabs.selenium.classic.core.BaseTest;
import ua.net.itlabs.selenium.classic.pages.GmailPage;

public class GmailTest extends BaseTest {

    GmailPage gmailPage = new GmailPage(driver);

    @Test
    public void testSendingMail() {

        String subject = Long.toString(System.currentTimeMillis() / 1000L);

        gmailPage.visit();
        gmailPage.signIn();

        find(gmailPage.composeButton).click();
        gmailPage.mailWidget.sendEmail("itlabs.tasjg5@gmail.com", subject);

        gmailPage.refreshEmailList();
        gmailPage.search(subject);
    }

}